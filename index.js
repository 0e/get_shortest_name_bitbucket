const request = require('request');
const Combinatorics = require('js-combinatorics');


const asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array)
    }
};

const delay = (ms) => {
    return new Promise(resolve => {
        timeoutId = setTimeout(function(){
            resolve(timeoutId);
        }, ms);
    })
};

const getRequestHeader = {
    "User-Agent": "Mozilla/1.0 (X11; Ubuntu; Linux x86_64; rv:64.0) Gecko/20100101 Firefox/64.0",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Upgrade-Insecure-Requests": "1",
    "Cache-Control": "max-age=0",
    'DNT':'1',
    'Host':'bitbucket.org',
};


(() => {

    // 'A',
    //     'B',
    //     'C',
    //     'D',
    //     'E',
    //     'F',
    //     'G',
    //     'H',
    //     'I',
    //     'J',
    //     'K',
    //     'L',
    //     'M',
    //     'N',
    //     'O',
    //     'P',
    //     'Q',
    //     'R',
    //     'S',
    //     'T',
    //     'U',
    //     'V',
    //     'W',
    //     'X',
    //     'Y',
    //     'Z',

    const Combs = Combinatorics.baseN([
        'a',
        'b',
        'c',
        'd',
        'e',
        'f',
        'g',
        'h',
        'i',
        'j',
        'k',
        'l',
        'm',
        'n',
        'o',
        'p',
        'q',
        'r',
        's',
        't',
        'u',
        'v',
        'w',
        'x',
        'y',
        'z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '_'
    ], 2);


    asyncForEach(Combs.toArray(), async (i) => {

        await new Promise( async (resolve, reject) => {

        request({
            url: `https://bitbucket.org/xhr/name_available/${i.join('')}`,
            headers: getRequestHeader
        }, (error, response, body) => {
            if(body !== 'false'){
                console.log(body, i.join(''));
            }

            if(error){
                console.error(error);
            }
            resolve(body);
        });

    });

     await delay(3000);
    })
})();